#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/init.h"
#include "threads/malloc.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "devices/input.h"
#define VADDR_BOTTOM ((void *) 0x08048000)

static struct lock filesys_lock;


struct file_info
{
	int fd_number;
	struct file *file;
	struct list_elem elem;
	char file_name[16];
};

static void syscall_handler (struct intr_frame *);
void syscall_exit(int status);
tid_t syscall_exec(const char *cmd_line);
int syscall_wait(tid_t tid);
bool syscall_create(const char *file, unsigned initial_size);
bool syscall_remove(const char *file);
int syscall_open(const char *file);
int syscall_filesize(int fd);
int syscall_read (int fd, void *buffer, unsigned size);
int syscall_write(int fd, const void *buffer, unsigned size);
void syscall_seek (int fd, unsigned position);
unsigned syscall_tell (int fd);
void syscall_close (int fd);
struct file* syscall_get_file(int fd);
char* syscall_get_name(int fd);
struct file_info* syscall_get_file_info(int fd);

void syscall_filesys_lock_acquire (void);
void syscall_filesys_lock_release (void);
void exit_macro();

void
syscall_init (void) 
{
	lock_init (&filesys_lock);
  	intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static bool syscall_ptr_checker(void *ptr)
{
	if (ptr != NULL && is_user_vaddr(ptr) 
		&& pagedir_get_page(thread_current() -> pagedir, ptr) != NULL && ptr > VADDR_BOTTOM)
		return true;
	else
	{
		syscall_exit(-1);
		return false;
	}
}

void exit_macro()
{
	thread_current()->exit_status = -1;
	printf("%s: exit(%d)\n", thread_current()->name, thread_current()->exit_status);
	thread_exit ();
}

void* user_address(void* vaddr)
{
	if(!is_user_vaddr(vaddr))
		syscall_exit(-1);

	void* ptr = pagedir_get_page(thread_current()-> pagedir, vaddr);
	return ptr;
}

static void
syscall_handler (struct intr_frame *f UNUSED) 
{
	ASSERT (f != NULL);
	ASSERT (f->esp != NULL);

	uint32_t *stack_ptr = f->esp;

	if(syscall_ptr_checker(stack_ptr))
	{
		int syscall_number = *stack_ptr;
		switch(syscall_number)
		{
			case SYS_HALT:
				power_off();
				break;
			case SYS_EXIT:
				if(syscall_ptr_checker(stack_ptr + 1))
					syscall_exit(*(stack_ptr + 1));
				else
					exit_macro();
				break;
			case SYS_EXEC:
				if(syscall_ptr_checker(stack_ptr + 1))
					f->eax = syscall_exec((char *) *(stack_ptr + 1));
				else
					exit_macro();
				break;
			case SYS_WAIT:
				if(syscall_ptr_checker(stack_ptr + 1))
					f->eax = syscall_wait(*(stack_ptr + 1));
				else
					exit_macro();
				break;
			case SYS_CREATE:
				if (syscall_ptr_checker (stack_ptr + 1) && syscall_ptr_checker (stack_ptr + 2))
              		f->eax = syscall_create ((char *) *(stack_ptr + 1), *(stack_ptr + 2));
              	else
              		exit_macro();
				break;
			case SYS_REMOVE:
				if (syscall_ptr_checker (stack_ptr + 1))
              		f->eax = syscall_remove ((char *) *(stack_ptr + 1));
              	else
              		exit_macro();
				break;
			case SYS_OPEN:
				if (syscall_ptr_checker (stack_ptr + 1))
              		f->eax = syscall_open(*(stack_ptr + 1));
              	else
              		exit_macro();
				break;
			case SYS_FILESIZE:
				if (syscall_ptr_checker (stack_ptr + 1))
              		f->eax = syscall_filesize(*(stack_ptr + 1));
              	else
              		exit_macro();
				break;
			case SYS_READ:
				if(syscall_ptr_checker(stack_ptr + 1) &&
					syscall_ptr_checker(stack_ptr + 2) &&
					syscall_ptr_checker(stack_ptr + 3))
					f->eax = syscall_read(*(stack_ptr + 1), (void *) *(stack_ptr + 2), *(stack_ptr + 3));
				else
					exit_macro();
				break;
			case SYS_WRITE:
				if(syscall_ptr_checker(stack_ptr + 1) &&
					syscall_ptr_checker(stack_ptr + 2) &&
					syscall_ptr_checker(stack_ptr + 3))
					f->eax = syscall_write(*(stack_ptr + 1), (void *) *(stack_ptr + 2), *(stack_ptr + 3));
				else
					exit_macro();
				break;
			case SYS_SEEK:
				if (syscall_ptr_checker (stack_ptr + 1) && syscall_ptr_checker (stack_ptr + 2))
              		syscall_seek(*(stack_ptr + 1), *(stack_ptr + 2));
              	else
              		exit_macro();
				break;
			case SYS_TELL:
				if (syscall_ptr_checker (stack_ptr + 1))
              		f->eax = syscall_tell(*(stack_ptr + 1));
              	else
              		exit_macro();
				break;
			case SYS_CLOSE:
				if (syscall_ptr_checker (stack_ptr + 1))
              		syscall_close(*(stack_ptr + 1));
              	else
              		exit_macro();
				break;
			default:
				exit_macro();
				break;
		}
	}
	else
		exit_macro();
  	
}

void syscall_exit(int status)
{
	struct thread *curr;

	curr = thread_current();
	curr->exit_status = status;

	printf("%s: exit(%d)\n", curr->name, curr->exit_status);

	thread_exit();
}

tid_t syscall_exec(const char *cmd_line)
{
	if(syscall_ptr_checker(cmd_line))
	{
		char *ptr = user_address(cmd_line);
		if(ptr == NULL)
			return -1;

		tid_t child_tid = process_execute(ptr);
		return child_tid;
	}

	return -1;
}

int syscall_wait(tid_t tid)
{
	return process_wait(tid);
}

bool syscall_create(const char *file, unsigned initial_size)
{
	bool ret;
	char *ptr;

	if(syscall_ptr_checker(file))
	{
		ptr = user_address(file);
		if(ptr == NULL)
			syscall_exit(-1);

		lock_acquire(&filesys_lock);
		ret = filesys_create (file, initial_size);
		lock_release(&filesys_lock);
		return ret;
	}
	else
	{
		syscall_exit(-1);
		return false;
	}
}

bool syscall_remove(const char *file)
{
	bool ret;
	char* ptr;

	if(syscall_ptr_checker(file))
	{
		ptr = user_address(file);
		if(ptr == NULL)
			syscall_exit(-1);

		lock_acquire(&filesys_lock);
		ret = filesys_remove (ptr);
		lock_release(&filesys_lock);
		return ret;
	}
	else
	{
		syscall_exit(-1);
		return false;
	}
}

int syscall_open(const char *file)
{
	struct file *f;
	int fd;
	char *ptr;

	if(syscall_ptr_checker(file))
	{
		ptr = user_address(file);
		if(ptr == NULL)
			syscall_exit(-1);

		lock_acquire(&filesys_lock);
		f = filesys_open(ptr);
		if(f == NULL)
		{
			lock_release(&filesys_lock);
			return -1;
		}

		lock_release(&filesys_lock);

		struct file_info *f_info = malloc(sizeof(struct file_info));
		f_info->fd_number = thread_current()->fd;
		f_info->file = f;
		strlcpy (f_info->file_name, file, sizeof f_info->file_name);
		list_push_back(&thread_current()->file_info_list, &f_info->elem);
		thread_current()->fd += 1;

		return f_info->fd_number;
	}
	else
		return -1;
}

int syscall_filesize(int fd)
{
	struct file *f;
	int ret;

	f = syscall_get_file(fd);

	if (f == NULL)
	{
		return -1;
	}

	lock_acquire(&filesys_lock);
	ret = file_length(f);
	lock_release(&filesys_lock);

	return ret;

}

struct file* syscall_get_file(int fd)
{
	struct thread *curr = thread_current();
	struct list_elem *e;
	struct file_info *f_info;

	for (e = list_begin(&curr->file_info_list); e != list_end(&curr->file_info_list); e = list_next(e))
	{
		f_info = list_entry(e, struct file_info, elem);
		if (fd == f_info->fd_number)
			return f_info->file;
	}

	return NULL;
}

char* syscall_get_name(int fd)
{
	struct thread *curr = thread_current();
	struct list_elem *e;
	struct file_info *f_info;

	for (e = list_begin(&curr->file_info_list); e != list_end(&curr->file_info_list); e = list_next(e))
	{
		f_info = list_entry(e, struct file_info, elem);
		if (fd == f_info->fd_number)
			return f_info->file_name;
	}

	return NULL;
}

struct file_info* syscall_get_file_info(int fd)
{
	struct thread *curr = thread_current();
	struct list_elem *e;
	struct file_info *f_info;

	for (e = list_begin(&curr->file_info_list); e != list_end(&curr->file_info_list); e = list_next(e))
	{
		f_info = list_entry(e, struct file_info, elem);
		if (fd == f_info->fd_number)
			return f_info;
	}

	return NULL;
}

int syscall_read (int fd, void *buffer, unsigned size)
{
	struct file *f;
	int ret;
	uint8_t* temp_buffer;
	char* ptr;

	if(syscall_ptr_checker(buffer))
	{
		ptr = user_address(buffer);
		if(ptr == NULL)
			syscall_exit(-1);

		if (fd == 0)
		{
			int i;
			for (i = 0; i < size; i++)
				((uint8_t *) buffer)[i] = input_getc();
	
			return size;
		}

		f = syscall_get_file(fd);
		if (f == NULL)
		{
			syscall_exit(-1);
		}
		
		lock_acquire(&filesys_lock);
		ret = file_read(f, ptr, size);
		lock_release(&filesys_lock);
		return ret;
	}
	else
	{
		syscall_exit(-1);
	}
}

int syscall_write(int fd, const void *buffer, unsigned size)
{
	struct file *f;
	struct file_info* f_info;
	int ret;
	void* ptr;

	if(syscall_ptr_checker(buffer))
	{
		ptr = user_address(buffer);
		if(ptr == NULL)
			syscall_exit(-1);

		if(fd == 1)
		{
			putbuf(ptr,size);
			return size;
		}
		
		f = syscall_get_file(fd);
		f_info = syscall_get_file_info(fd);
		if (f == NULL)
			syscall_exit(-1);

		if(thread_find_filename(f_info->file_name))
			return 0;

		lock_acquire(&filesys_lock);
		ret = file_write(f, ptr, size);
		lock_release(&filesys_lock);
		return ret;
	}
	else
		syscall_exit(-1);
}

void syscall_seek (int fd, unsigned position)
{
	struct file *f;

	f = syscall_get_file(fd);
	if(f == NULL)
	{
		return;
	}

	lock_acquire(&filesys_lock);
	file_seek(f, position);
	lock_release(&filesys_lock);
}

unsigned syscall_tell (int fd)
{
	struct file *f;
	off_t ret;

	f = syscall_get_file(fd);
	if(f == NULL)
	{
		return -1;
	}

	lock_acquire(&filesys_lock);
	ret = file_tell(f);
	lock_release(&filesys_lock);
	return ret;
}

void syscall_close (int fd)
{
	struct thread *curr;
	struct list_elem *e;
	struct file_info *f_info;
	bool file_found = false;

	curr = thread_current();

	for(e = list_begin(&curr->file_info_list); e != list_end(&curr->file_info_list); e = list_next(e))
	{
		f_info = list_entry(e, struct file_info, elem);
		if (fd == f_info->fd_number)
		{
			lock_acquire(&filesys_lock);
			file_close(f_info->file);
			lock_release(&filesys_lock);
			list_remove(&f_info->elem);
			free(f_info);
			file_found = true;
			break;
		}
	}

	if(!file_found)
		syscall_exit(-1);
}